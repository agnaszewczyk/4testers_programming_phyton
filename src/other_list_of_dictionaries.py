# Exercise on postal adresses

if __name__ == '__main__':
    addresses = [
        {
            "city": "Gdynia",
            "street": "Pomorska",
            "house_number": "42",
            "post_code": "12-345"
        },
        {
            "city": "Opole",
            "street": "Opolska",
            "house_number": "76",
            "post_code": "22-345"
        },
        {
            "city": "Katowice",
            "street": "Niewiadomska",
            "house_number": "61",
            "post_code": "50-345"
        }
    ]
    print(addresses[-1]["post_code"])
    print(addresses[1]["city"])
    addresses[0]["street"] = "Zabawowa"
    print(addresses)

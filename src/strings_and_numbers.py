first_name = "Agnieszka"
last_name = "Szewczyk"
email = "agnaszewczyk@wp.pl"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}"
print(my_bio_using_f_string)

print(f"Wynik operacji mnnożenia 4 przez 5 to {4*5}")


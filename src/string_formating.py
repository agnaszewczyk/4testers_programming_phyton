## Exercise 1
def print_hello_message(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")

print_hello_message("Michał", "Toruń")
print_hello_message("beata", "gdynia")

## Exercise 2

def get_email_address(name,last_name):
    return f"{name.lower()}.{last_name.lower()}@4testers.pl"


print(get_email_address("Janusz", "Nowak"))
print(get_email_address("Barbara", "Kowalska"))
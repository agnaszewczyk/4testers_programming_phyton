def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius > 0:
        print("It's quite OK")
    else:
        print("It's getting cold")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False
    # return True if age >= 18 else False


if __name__ == '__main__':
    temperature_in_celsius = 26
    print_temperature_description(temperature_in_celsius)

age_kate = 17
age_tom = 18
age_marta = 21
print("Kate", is_person_an_adult(age_kate))
print("Tom", is_person_an_adult(age_tom))
print("Marta", is_person_an_adult(age_marta))


# Name length

def print_word_name_length_description(name):
    if len(name) > 5:
        print(f"The name {name} is longer than 5 characters")
    else:
        print(f"The name {name} is 5 characters or shorter")

if __name__ == '__main__':
    long_name = "Agnieszka"
    short_name = "Ada"
    print_word_name_length_description(long_name)
    print_word_name_length_description(short_name)

# Celsius and Hectopascal
def check_if_normal_condition(celsius, pressure):
    if (celsius == 0) and (pressure == 1013):
        return True
    else:
        return False

if __name__ == '__main__':

    print(check_if_normal_condition(0, 1013))
    print(check_if_normal_condition(1, 1014))
    print(check_if_normal_condition(0, 1014))
    print(check_if_normal_condition(1, 1014))

# Grades
def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2

if __name__ == '__main__':
    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))

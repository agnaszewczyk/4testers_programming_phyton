# Exercise 1
def get_number_squared(num):
    return num ** 2


zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)


# Exercise 2
def cuboid_volume(a, b, c):
    return a * b * c


print(cuboid_volume(3, 5, 7))


# Exercise 3
def convert_celsius_to_fahrenheit(temp_celsius):
    return (temp_celsius * 9 / 5) + 32


temp_celsius = 20
print(f"Celsius = {temp_celsius}, Fahrenheit = {convert_celsius_to_fahrenheit(temp_celsius)}")

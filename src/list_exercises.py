movies = ["Who wants what", "If fish could talk", "Flawless", "Have a dring", "Tea or Fish"]
first_movie = movies[0]
print(f"first_movie is {first_movie}")

print(f"Last_name is {movies[-1]}")

movies.append("Dune")

print(f"The length of movie list is {len(movies)}")

movies.insert(0, "Star Wars")
movies.remove("Flawless")

print(movies)

movies[-1] = "Dune (1980)"
print(movies)

### Exercise - list
emails = ["a@example.com", "b@example.com"]

print(len(emails))
print(emails[0])
print(emails[-1])

emails.append("cde@example.com")

## Exercise - dictionary

friend = {
    "name": "Marvin",
    "age": 33,
    "hobby": ["shark diving", "chess"]
}
print(friend["age"])
friend["city"] = "Wrocław"
friend["age"] = 39

print(friend)
friend["job"] = "doctor"
print(friend)
del friend["job"]
print(friend)

print(friend["hobby"][-1])
friend["hobby"].append("climbing")
print(friend)

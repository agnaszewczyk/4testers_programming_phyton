my_name = "Agnieszka"
my_age = 70
my_email = "agnaszewczyk@wp.pl"

print(my_name)
print(my_age)
print(my_email)

# Below is the description of my friend

my_friends_name = "Anielka"
my_friends_age = 40
my_friends_has_pets = 3
my_friend_has_driving_licence = True
friendship_time = 7.5

print(my_friends_name, my_friends_age, my_friends_has_pets, my_friend_has_driving_licence, friendship_time, sep ="\t")


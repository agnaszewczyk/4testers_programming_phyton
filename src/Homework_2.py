import uuid


def get_list_element_sum(list_with_numbers):
    return sum(list_with_numbers)


temperatures_in_january = [-4, 1.0, -7, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f"January: {get_list_element_sum(temperatures_in_january)}")
print(f"February: {get_list_element_sum(temperatures_in_february)}")


def get_average_of_list(numbers_list):
    return sum(numbers_list) / len(numbers_list)


def get_average_of_two_num(no1, no2):
    return (no1 + no2) / 2


def run_exercise_1():
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]

    average_temperature = get_average_of_two_num(get_average_of_list(january), get_average_of_list(february))
    print('Average temperature in january and february:', average_temperature)


def get_user_credentials(email):
    user_credentials_dict = {"email": email, "password": str(uuid.uuid4())}
    return user_credentials_dict


def run_exercise_2():
    print(f'\nUser credentials: {get_user_credentials("user@example.com")}')
    print(f'\nUser credentials: {get_user_credentials("user@example.pl")}')


def print_gamer_description(gamer_dictionary):
    print(
        f"The player {gamer_dictionary['nick']} is of type {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']} EXP")


def run_exercise_3():
    player_1 = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    player_2 = {
        "nick": "omnibus_54",
        "type": "mage",
        "exp_points": 99999
    }
    print_gamer_description(player_1)
    print_gamer_description(player_2)

if __name__ == '__main__':
    run_exercise_1()
    run_exercise_2()
    run_exercise_3()

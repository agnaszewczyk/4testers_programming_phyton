def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    for integer in range(1, 31):
        print(integer ** 2)


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "toSia", "niCkI", "STEPHANE"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()

# uuid and numbers multiply and devisible
import uuid
def print_random_uuids(number_of_strings):
    for id in range(number_of_strings):
        print(str(uuid.uuid4()))

def print_numbers_from_20_to_30_multiplied_by_4():
    for number in range(20, 31):
        print(number*4)

def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number+1):
        if number % 7 == 0:
            print(number)

def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp *9 /5 +32
        print(f"Celsius: {temp}, Fahrenheit: {temp_fahrenheit}")

def get_temperatures_higher_than_20_degrees(list_of_temps_in_celsius):
    filtered_temperatures = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temperatures.append(temp)
    return filtered_temperatures

if __name__ == '__main__':
    print_random_uuids(7)
    print_numbers_from_20_to_30_multiplied_by_4()
    print_numbers_divisible_by_7(1, 30)

    temp_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temp_celsius)

    print(get_temperatures_higher_than_20_degrees(temp_celsius))
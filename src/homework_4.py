import random

# Login data list
female_fnames = ["Kate", "Agnieszka", "Anna", "Maria", "Joss", "Eryka"]
male_fnames = ["James", "Bob", "Jan", "Hans", "Orestes", "Saturnin"]
surnames = ["Smith", "Kowalski", "Yu", "Bona", "Muster", "Skinner", "Cox", "Brick", "Malina"]
countries = ["Poland", "United Kingdom", "Germany", "France", "Other"]


def generate_person_dictionary(is_female):
    if is_female:
        generate_fname = random.choice(female_fnames)
    else:
        generate_fname = random.choice(male_fnames)
    generate_surname = random.choice(surnames)
    generate_email = f"{generate_fname.lower()}, {generate_surname.lower()}@example.com"
    generate_age = random.randint(5, 45)
    generate_country = random.choice(countries)
    adult = generate_age >= 18
    year_of_birth = 2023 - generate_age
    return {
        "firstname": generate_fname,
        "lastname": generate_surname,
        "email": generate_email,
        "age": generate_age,
        "country": generate_country,
        "is_adult": adult,
        "birth_year": year_of_birth
    }


def generate_list_of_people_with_5_female_names_and_5_male_names():
    list_of_people = []
    for i in range(10):
        if i % 2 == 0:  # jeśli podzielne przez 2 to do listy jest dopisywany słownik z imieniem żeńskim
            list_of_people.append(generate_person_dictionary(True))
        else:
            list_of_people.append(generate_person_dictionary(False))
    return list_of_people


def print_description_for_every_person_from_the_list(list_of_people):
    for person in list_of_people:
        print(f"Hi! I am {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}")


if __name__ == '__main__':
    list_of_people = generate_list_of_people_with_5_female_names_and_5_male_names()
    print_description_for_every_person_from_the_list(list_of_people)
